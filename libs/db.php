<?php

    $dbhost = 'localhost';
    $dbuser = 'root';
    $dbpass = '';
    $dbname = 'login_capt';

    $dbcon = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
    
    if($dbcon->connect_errno){
        echo "Error! Database Connection Failed".$dbcon->connect_error();
    }